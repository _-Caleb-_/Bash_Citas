#!/bin/bash
#Citas célebres para bash
#Versión 0.9
#2022 por _-Caleb-_
#Telegram: @calebin
#/------------------------------------/

#Definiendo Colores
Rosa='\033[0;35m' 	#Rosa
Naranja='\033[0;33m' 	#Naranja
Azul='\033[1;36m'	#Azul (Negrita, el 1 de 1;36 es el que establece si es negrita o no)
AzulFlojo='\033[0;36m'	#Azul
SC='\033[0m' 		#Sin color

if [ -f $HOME/.config/citas/citas ];		        #Comprobamos que el archivo de citas existe.
then		                                        #Si existe
totalcitas=$(cat $HOME/.config/citas/citas | wc -l)	#Contamos el número de líneas del archivo.
numerocita=$(shuf -i 1-$totalcitas -n 1)		#Randomizamos la cita que se va a leer entre 1 y el número total de líneas del archivos
numerocolor=$(shuf -i 1-3 -n 1)		                #Randomizamos el color que se va a mostrar entre 1 y 3, que corresponden a los colores definidos al principio del Script
    case $numerocolor in
      1)
        numerocolor=$Rosa
      ;;
      2)
        numerocolor=$Naranja
      ;;
      3)
        numerocolor=$AzulFlojo
      ;;
    esac
cita=$(awk 'NR == '$numerocita'' "$HOME/.config/citas/citas")	#Definimos el número de línea que se va a leer del archivo
    echo ""							#
    echo -e ${Azul}"   Cita:"${SC}		                #Muestra la cita
    echo -e "   "${numerocolor}$cita${SC}
else								#Si no encuentra el archivo de citas
								#Declaramos variable de directorio y archivo de citas y mostramos unos mensajes para que el user sepa qué se está haciendo
echo ">> No se encuentra el archivo de citas en" $HOME/.config/citas/citas ", creando uno"
dircitas="$HOME/.config/citas/"
archivocitas="$HOME/.config/citas/citas"
mkdir $dircitas		#Creamos el directorio
touch $archivocitas	#Creamos el archivo de citas
			#Añadamos algunas citas al archivo
	echo "No hay hombre tan cobarde a quien el amor no haga valiente y transforme en héroe. Platón." >> $archivocitas
	echo "Hay alguien tan inteligente que aprende de la experiencia de los demás. Voltaire." >> $archivocitas
	echo "No puedo enseñar nada a nadie. Solo puedo hacerles pensar. Sócrates." >> $archivocitas
	echo "Vivir sin filosofar es, propiamente, tener los ojos cerrados, sin tratar de abrirlos jamás. René Descartes." >> $archivocitas
	echo "Lo menos frecuente en este mundo es vivir. La mayoría de la gente existe, eso es todo. Oscar Wilde." >> $archivocitas
	echo "La felicidad de tu vida depende de la calidad de tus pensamientos. Marco Aurelio. " >> $archivocitas
	echo "La vida no se trata de encontrarte a ti mismo, sino de crearte a ti mismo. Bernard Shaw. " >> $archivocitas
	echo "Pensar es fácil, actuar es difícil, y poner los pensamientos de uno mismo en acción es lo más difícil del mundo. Goethe. " >> $archivocitas
	echo "Felicidad no es hacer lo que uno quiere sino querer lo que uno hace. Jean Paul Sartre." >> $archivocitas
	echo "No lastimes a los demás con lo que te causa dolor a ti mismo. Buda." >> $archivocitas
	echo "El hombre que mueve montañas empieza apartando piedras pequeñas. Confucio." >> $archivocitas
	echo "Educa a los niños y no será necesario castigar a los hombres. Pitágoras de Samos. " >> $archivocitas
	echo "Los hombres geniales empiezan grandes obras, los hombres trabajadores las terminan. Leonardo Da Vinci." >> $archivocitas
	echo "La verdadera libertad consiste en el dominio absoluto de sí mismo. Galileo Galilei." >> $archivocitas
	echo "El regalo más grande que les puedes dar a los demás es el ejemplo de tu propia vida. Bertolt Brecht." >> $archivocitas
	echo "Me he dado cuenta que incluso las personas que dicen que todo está predestinado y que no podemos hacer nada para cambiar nuestro destino igual miran antes de cruzar la calle. Stephen  Hawking. " >> $archivocitas
	echo "Vivir es nacer a cada instante. Erich Fromm." >> $archivocitas
	echo "No malgastes tu tiempo, pues de esa materia está formada la vida. Benjamin Franklin. " >> $archivocitas
	echo "¿Quieres ser rico? Pues no te afanes en aumentar tus bienes sino en disminuir tu codicia. Epicuro." >> $archivocitas
	echo "Las que conducen y arrastran al mundo no son las máquinas, sino las ideas. (Victor Hugo)" >> $archivocitas
	echo "Allí donde reinan la quietud y la meditación, no hay lugar para las preocupaciones ni para la disipación. Francisco de Asís." >> $archivocitas
	echo "La vida debe ser comprendida hacia atrás. Pero debe ser vivida hacia delante. Kierkegaard." >> $archivocitas
	echo "Todo el que disfruta cree que lo que importa del árbol es el fruto, cuando en realidad es la semilla. He aquí la diferencia entre los que crean y los que disfrutan. Friedrich Nietzsche. " >> $archivocitas
	echo "Al final no son los años en nuestra vida lo que cuenta, sino la vida en nuestros años. Abraham Lincoln. " >> $archivocitas
	echo "Hay tres frases que nos impiden avanzar: tengo que hacerlo bien, me tienes que tratar bien y el mundo debe ser fácil. Albert Ellis. " >> $archivocitas
	echo "La buena conciencia es la mejor almohada para dormir. Sócrates." >> $archivocitas
	echo "Todo el mundo ve lo que aparentas ser, pocos experimentan lo que realmente eres. Maquiavelo." >> $archivocitas
	echo "No es lo que te ocurre, sino cómo reaccionas lo que importa. Epíteto." >> $archivocitas
	echo "Iré a cualquier parte, siempre que sea hacia adelante. David Livingstone. " >> $archivocitas
	echo "Qué pequeña eres brizna de hierba. Sí, pero tengo toda la Tierra a mis pies. Rabindranath Tagore." >> $archivocitas
	echo "Es intentando lo imposible como se realiza lo posible. Henri Barbusse." >> $archivocitas
	echo "Quien sólo vive para sí, está muerto para los demás. Publio Siro." >> $archivocitas
	echo "Si de algo soy rico es de perplejidades y no de certezas. Jorge Luis Borges." >> $archivocitas
	echo "El único encanto del pasado consiste en que es el pasado. Oscar Wilde." >> $archivocitas
	echo "El exceso es el veneno de la razón. Francisco de Quevedo." >> $archivocitas
	echo "La fe es el antiséptico del alma. Walt Whitman." >> $archivocitas
	echo "La belleza sólo le pertenece al que la entiende, no al que la tiene. Carlos Fuentes." >> $archivocitas
	echo "Nullum Gratuitum Prandium. Anónimo" >> $archivocitas

echo ">> Creado, no volverás a ver este mensaje."	# Mostramos un mensaje final
sleep 2							# 2 Segundos de espera y listo
fi
#EOF
