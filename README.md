# Bash_Citas

Script Bash que muestra una cita al azar en un color aleatorio de 3 disponibles en la terminal.

La primera vez que se ejecuta crea el archivo *$HOME/.config/citas/citas* y le añade algunas citas ya incluídas en el script. 

## Requisitos: 
- shuf
- awk

![](https://codeberg.org/_-Caleb-_/todo-bash/raw/branch/master/images/Captura%20de%20pantalla%20de%202022-07-10%2010-33-38.png)

![](https://codeberg.org/_-Caleb-_/Bash_Citas/raw/branch/main/img/screenshot5.png)